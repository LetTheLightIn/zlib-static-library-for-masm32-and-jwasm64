# Zlib static library for Masm32 and JWasm64

fearless 2015 - [www.LetTheLight.in](http://www.LetTheLight.in)

## Overview

Compiled static library of the zlib source for Masm32 and Jwasm64

## Whats included in this package
* zlibstat128_x32.inc - Zlib Include file for use with Masm32
* zlibstat128_x32.lib - Zlib Library file for use with Masm32
* zlibstat128_x64.inc - Zlib Include file for use with JWasm64
* zlibstat128_x64.lib - Zlib Library file for use with JWasm64
* ZlibForMasm32.zip - Contains just the .inc and .lib for Masm32
* ZlibForJWasm64.zip - Contains just the .inc and .lib for JWasm64
* ZlibForBothAssemblers.zip - Contains both zipped Zlib packages.

##Installation for use with Masm32
* Copy zlibstat128_x32.inc to \Masm32\include folder
* Copy zlibstat128_x32.lib to \Masm32\x64 folder
* Include the Zlib files in your Masm32 project
```
    include zlibstat128_x32.inc
    includelib zlibstat128_x32.lib
```

##Installation for use with JWasm64
* Copy zlibstat128_x64.inc to \JWasm64\include folder
* Copy zlibstat128_x64.lib to \JWasm64\x64 folder
* Include the Zlib files in your JWasm64 project
```
    include zlibstat128_x64.inc
    includelib zlibstat128_x64.lib
```

##Notes
* Compiled with Visual Studio v10 from zlib v1.2.8 sources
* zlib was written by Jean-loup Gailly (compression) and Mark Adler (decompression). [www.zlib.net](http://www.zlib.net)

## Other downloads
 * [RadASM IDE v2.2.1.6](http://www.oby.ro/rad_asm/)
 * [RadASM IDE v2.2.2.0](http://www.assembly.com.br/download/radasm.html)
 * [JWasm/JWasm64](http://masm32.com/board/index.php?topic=3795.0)
 * [Masm64 Library](https://bitbucket.org/LetTheLightIn/masm64-library)


